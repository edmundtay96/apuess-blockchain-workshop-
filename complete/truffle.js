/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */
var PrivateKeyProvider = require("truffle-privatekey-provider");
var privateKey = '899c0d4f02957f641daebda94d9bba917fa176bc9e24261083071ecb4e282e85';
var provider = new PrivateKeyProvider(privateKey, "http://127.0.0.1:22000");
module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 22000,
      network_id: "*", // Match any network id
      provider: provider,
    }
  }
};
